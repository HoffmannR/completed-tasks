var app = angular.module('searchApp', []);

app.controller('searchCtrl', function($scope, $http) {
    $scope.search = function () {
        $http.post('search.php', {'data' : $scope.input_data})
            .then(function(response) {
                $scope.result = response.data;
            }, function(response) {
                $scope.result = response.status + ' ' + response.statusText;
            });
    }
});