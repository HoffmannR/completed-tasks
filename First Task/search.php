<?php
$json_data = file_get_contents("php://input");
$obj_data = json_decode($json_data);

$url_data = urlencode($obj_data->data);
$url =  "https://api.cognitive.microsoft.com/bing/v5.0/search?q=$url_data&count=1";

$accountKey = '42922786c0134a9783899b0e35d282a3';
$opts = [
    'http'=>[
        'method'=>"GET",
        'header'=>"Ocp-Apim-Subscription-Key: $accountKey",
    ],
];
$context = stream_context_create($opts);

$json_results = file_get_contents($url, false, $context);
$obj_results = json_decode($json_results);

$selected_results = [
    'name' => $obj_results->webPages->value[0]->name,
    'url' => $obj_results->webPages->value[0]->url,
    'display_url' => $obj_results->webPages->value[0]->displayUrl,
];

echo json_encode($selected_results);