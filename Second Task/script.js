var app = angular.module('FileComparerApp', []);

app.controller('FileComparerCtrl', function($scope, $http) {
    $scope.selected = function(func_name) {
        $http.post('file_comparer.php', {'data':func_name})
            .then(function(response) {
                $scope.result = response.data;
            }, function(response) {
                $scope.result = response.status + ' ' + response.statusText;
            });
    }
});