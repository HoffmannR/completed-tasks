<?php

function Mode1($input_row, $patterns_row)
{
    return trim($input_row) == trim($patterns_row);
}

function Mode2($input_row, $patterns_row)
{
    $trimmed_patterns_row = trim($patterns_row);
    return preg_match("/$trimmed_patterns_row/i", trim($input_row));
}

function Mode3($input_row, $patterns_row)
{
    return levenshtein(trim($input_row), trim($patterns_row)) <= 1;
}

$json_data = file_get_contents("php://input");
$obj_data = json_decode($json_data);

$compare_mode = $obj_data->data;
$result = [];

$input = fopen('input.txt', 'r');
$patterns = fopen('patterns.txt', 'r');

while (($input_row = fgets($input)) !== false)
{
    while (($patterns_row = fgets($patterns)) !== false)
    {
        if($compare_mode($input_row, $patterns_row))
        {
            $result[] = $input_row;
        }
    }
    rewind($patterns);
}

fclose($input);
fclose($patterns);

echo json_encode($result);